//created a class.
class User {
  String nameoftheapp;
  String nameofdeveloper;
  String categorywon;
  int year;

  User(this.nameoftheapp, this.nameofdeveloper, this.categorywon, this.year);

  toString() {
    return ('The $nameoftheapp app, developed by $nameofdeveloper, won an award in the $categorywon sector, in $year.');
  }
}

void main() {
//created an object to print the name of the app, sector/category, developer and the year it won MTN Business App of the Year Awards.
//created a function inside the class, that transforms the app name to all capital letters, printed the output.  
  print("*********************************************************************************************************************************");
  List<User> users = [
    User('FNB Banking'.toUpperCase(),'FNB Connect','consumer', 2012),
    User('Snapscan'.toUpperCase(),'Gerrit Greef and Kobus Ehlers','consumer', 2013),
    User('Live Inspect'.toUpperCase(),'Lightstone', 'consumer', 2014),
    User('Wumdrop'.toUpperCase(),'Simon Hartley','enterprise', 2015),
    User('Domestly'.toUpperCase(),'Thatoyoana Marumo and Berno Potgieters','consumer', 2016),
    User('Standard Bank Shyft'.toUpperCase(),'Arno von Helden','finance', 2017),
    User('Khula'.toUpperCase(),'Karidas Tshintsholo and Matthew Piper','consumer', 2018),
    User('Naked Insurance'.toUpperCase(),'Sumarie Greybe, Ernest North and Alex Thomson','finance', 2019),
    User('Easy Equities'.toUpperCase(),'Charles Savage','investment', 2020),
    User('Ambani'.toUpperCase(),'Mukundi Lambani','education', 2021)
  ];
  users.sort((a, b) => a.year.compareTo(b.year));
  for(var users in users)
  print('${users.toString()}');
print("**********************************************************************************************************************************");
}